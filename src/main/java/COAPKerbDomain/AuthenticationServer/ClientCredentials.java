package COAPKerbDomain.AuthenticationServer;

import COAPKerbDomain.KerbContent;
import COAPKerbDomain.TicketGrantingTicket;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;

import java.io.IOException;

public class ClientCredentials extends KerbContent{

    public String tgsSessionKey;
    public int statusCode;

    public void setTGSSessionKey(String tgsSessionKey) {
        this.tgsSessionKey = tgsSessionKey;
    }

    public void setTGT(TicketGrantingTicket tgt, String tgtKey) {
        super.setEncryptedCredential(tgt,tgtKey);
    }

    public static ClientCredentials toObject(byte[]  cborObject) {
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        ClientCredentials object = null;
        try {
            object = objectMapper.readValue(cborObject, ClientCredentials.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return object;
    }
}
