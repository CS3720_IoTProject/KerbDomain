package COAPKerbDomain.AuthenticationServer;

import COAPKerbDomain.AES;
import COAPKerbDomain.KerbEnvelope;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;
import java.io.IOException;

public class ClientTGTRequest extends KerbEnvelope{

    public String userName;

    public ClientTGTRequest(){}

    public ClientTGTRequest(String userName) {
        this.userName = userName;
    }

    public void setUserSignature(String userIP,String password) throws Exception {
        this.setContent(userIP,password);
    }

    public String getUserSignature(String password) throws Exception {
        return new String(AES.decryptBytes(this.encryptedContent,password));
    }

    public static ClientTGTRequest toObject(byte[] cborObject){
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        ClientTGTRequest userTicket = null;
        try {
            userTicket = objectMapper.readValue(cborObject, ClientTGTRequest.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userTicket;
    }

}
