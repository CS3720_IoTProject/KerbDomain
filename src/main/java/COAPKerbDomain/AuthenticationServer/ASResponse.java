package COAPKerbDomain.AuthenticationServer;

import COAPKerbDomain.AES;
import COAPKerbDomain.KerbEnvelope;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;

import java.io.IOException;

public class ASResponse extends KerbEnvelope{

    public ClientCredentials getClientCredential (String password) throws Exception {
        byte [] clientCredCBOR = AES.decryptBytes(encryptedContent,password);
        return ClientCredentials.toObject(clientCredCBOR);
    }

    public static ASResponse toObject(byte[]  cborObject) {
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        ASResponse authResponse = null;
        try {
            authResponse = objectMapper.readValue( cborObject , ASResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return authResponse;
    }


}
