package COAPKerbDomain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;

import java.io.IOException;

public class KerbEnvelope extends CBORMessage{

    public byte[] encryptedContent;
    public byte[] encryptedCrendential;

    public void setContent(KerbContent content, String password) throws Exception {
        this.encryptedContent = AES.encryptBytes(content.toCBOR(),password);
    }

    public void setContent(String content, String password) throws Exception {
       this.encryptedContent = AES.encryptBytes(content.getBytes(),password);
    }

    public void setCrendential(ResourceAccessTicket content, String resKey) throws Exception {
        this.encryptedContent = AES.encryptBytes(content.toCBOR(),resKey);
    }

    public GenericResourceData getResData(String resKey) throws Exception {

        ResourceAccessTicket resTicket =  ResourceAccessTicket.toObject(AES.decryptBytes(encryptedCrendential,resKey));
        GenericResourceData genResData = GenericResourceData.toObject(AES.decryptBytes(encryptedContent,resTicket.resourceSessionKey));

        return genResData;
    }


    public static KerbEnvelope toObject(byte[] cborObject) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        KerbEnvelope envelope = objectMapper.readValue(cborObject,KerbEnvelope.class);
        return envelope;
    }

}
