package COAPKerbDomain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;


public abstract class CBORMessage {

    public byte[] toCBOR() throws JsonProcessingException {
        byte[]  cborObject = null;
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        cborObject = objectMapper.writeValueAsBytes(this);
        return cborObject;
    }

}
