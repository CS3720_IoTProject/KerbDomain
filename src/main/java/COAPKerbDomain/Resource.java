package COAPKerbDomain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;

import java.io.IOException;

public class Resource {

    public String resourceName;
    public String resourceCOAPURI;
    public String resourceKerberizedURI;
    public String resourceKey;

    public Resource() {
    }

    public static Resource toObject(byte[] cborObject) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        Resource res = objectMapper.readValue(cborObject,Resource.class);
        return res;
    }
}
