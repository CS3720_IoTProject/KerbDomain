package COAPKerbDomain;

public abstract class KerbContent extends CBORMessage{

    //TODO add unique ID generation for supporting replay cache
    public int UUID;

    public byte[] encryptedCredential;

    public void setCredential(byte[] encryptedCredential) {
        this.encryptedCredential = encryptedCredential;
    }

    public ResourceAccessTicket getResTicket(String resourceKey) throws Exception {
        return ResourceAccessTicket.toObject(AES.decryptBytes(encryptedCredential,resourceKey));
    }

    public void setEncryptedCredential(ResourceAccessTicket ticket, String password) {
        try {
            this.encryptedCredential = AES.encryptBytes(ticket.toCBOR(),password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
