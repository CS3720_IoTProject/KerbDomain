package COAPKerbDomain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;

import java.io.IOException;

public class ResourceAccessTicket extends CBORMessage{

    public String resourceName;
    public String resourceSessionKey;

    public ResourceAccessTicket(){
    }

    public ResourceAccessTicket(String resourceName) {
        this.resourceName = resourceName;
    }

    public static ResourceAccessTicket toObject(byte[] cborObject) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        ResourceAccessTicket userTicket = objectMapper.readValue(cborObject, ResourceAccessTicket.class);
        return userTicket;
    }
}
