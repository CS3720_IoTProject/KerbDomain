package COAPKerbDomain.TicketGrantingServer;

import COAPKerbDomain.AES;
import COAPKerbDomain.KerbEnvelope;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;

import java.io.IOException;

public class TGSResponse extends KerbEnvelope{

    public static TGSResponse toObject(byte[] cborObject) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        TGSResponse tgsResponse = objectMapper.readValue(cborObject, TGSResponse.class);
        return tgsResponse;
    }

    public TGSResponseContent getContent (String tgsKey) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        TGSResponseContent content = objectMapper.readValue(AES.decryptBytes(encryptedContent,tgsKey), TGSResponseContent.class);
        return content;
    }

}
