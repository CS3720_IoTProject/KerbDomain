package COAPKerbDomain.TicketGrantingServer;

import COAPKerbDomain.AES;
import COAPKerbDomain.KerbContent;
import COAPKerbDomain.TicketGrantingTicket;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;

import java.io.IOException;

public class TGSRequestContent extends KerbContent {

    public String resource;

    public TicketGrantingTicket getTGT(String tgtKey) throws Exception {
        return TicketGrantingTicket.toObject(AES.decryptBytes(encryptedCredential,tgtKey));
    }

    public static TGSRequestContent toObject(byte[] cborObject) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        TGSRequestContent tgsCred = objectMapper.readValue(cborObject, TGSRequestContent.class);
        return tgsCred;
    }


}
