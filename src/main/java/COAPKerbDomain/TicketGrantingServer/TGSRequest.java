package COAPKerbDomain.TicketGrantingServer;

import COAPKerbDomain.AES;
import COAPKerbDomain.KerbEnvelope;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;

import java.io.IOException;

public class TGSRequest extends KerbEnvelope {

    public TGSRequestContent getContent(String tgsKey) throws Exception {
        return TGSRequestContent.toObject(AES.decryptBytes(encryptedContent,tgsKey));
    }

    public static TGSRequest toObject(byte[] cborObject) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        TGSRequest tgsReq = objectMapper.readValue(cborObject,TGSRequest.class);
        return tgsReq;
    }
}
