package COAPKerbDomain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;

import java.io.IOException;

public class GenericResourceData extends KerbContent{

    public String data;

    public static GenericResourceData toObject(byte[] cborObject) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        GenericResourceData resourceData = objectMapper.readValue(cborObject,GenericResourceData.class);
        return resourceData;
    }

}
