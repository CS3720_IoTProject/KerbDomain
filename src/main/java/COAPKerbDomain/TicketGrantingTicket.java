package COAPKerbDomain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;

import java.io.IOException;

public class TicketGrantingTicket extends ResourceAccessTicket{


    public TicketGrantingTicket() {
    }

    public TicketGrantingTicket(String name) {
        super.resourceName = name;
    }

    public static TicketGrantingTicket toObject(byte[] cborObject) {
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        TicketGrantingTicket userTicket = null;
        try {
            userTicket = objectMapper.readValue(cborObject, TicketGrantingTicket.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userTicket;
    }


}
